function printUserInfo(fname,lname,age) {

	// Golden Profile
	console.log(`First Name: ${fname}\nLast Name: ${lname}\nAge: ${age}`)

	// Hobbies
	console.log("Hobbies:")
	let hobbies = ["Biking", "Swimming", "Hiking"];
	console.log(hobbies);

	// Work Address
	console.log("Work Address:")
	let workAddress = {
		houseNumber:"32",
		street:"Washinton",
		city:"Lincoln",
		state:"Nebraska"	
	};
	console.log(workAddress);


	// Age of John
	console.log(`${fname}${lname} is 30 years of age.\n`);
	
	// Hobbies of John
	console.log(`This was printed inside of this function`);
	console.log(hobbies);	

	// Address of John
	console.log(`This was printed inside of this function`);
	console.log(workAddress);

	let isMarried = true;
	console.log(`The value of isMarried is: ${isMarried}`);

};

printUserInfo('John','Smith','30');


function returnFunction() {
	let isMarried = false;
	return `The RETURN VALUE of isMarried is: ${isMarried}`;
};

let result = returnFunction();
console.log(result);


// STRETCH GOALS----------------


//Add two Numbers

const addNum1 = parseInt(prompt('Get the Sum: Enter the first number '));
const addNum2 = parseInt(prompt('Get the Sum: Enter the second number '));

const sum = addNum1 + addNum2;

console.log(`The sum of ${addNum1} and ${addNum2} is ${sum}`);



//Subsctract two Numbers

const subNum1 = parseInt(prompt('Get the Difference: Enter the first number '));
const subNum2 = parseInt(prompt('Get the Difference: Enter the second number '));

const difference = subNum1 - subNum2;

console.log(`The difference of ${subNum1} and ${subNum2} is ${difference}`);


//Subsctract two Numbers

const prodNum1 = parseInt(prompt('Get the Product: Enter the first number '));
const prodNum2 = parseInt(prompt('Get the Product: Enter the second number '));

const product = prodNum1 * prodNum2;

console.log(`The product of ${prodNum1} and ${prodNum2} is ${product}`);