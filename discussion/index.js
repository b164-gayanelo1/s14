
// Statements, Syntax and Comments

// JS statements usually end with semicolon(;). Are not required but we will use it to help us train to locate where a statement ends.

	// console.log : Prints out a statement inside the parenthesis.

console.log("Hello world");

// Note : Breaking long statements in to multiple lines is common practice.

// Variables : It is used to contain data

// Declare Variables:
	// Syntax : let/const/var variableName;
	// let/const/var is a keyword that is usually used in declaring a variable.

const myVariable = "Hello again";
// ^ variable > variable name > assignment operator > string
console.log(myVariable);

/*
	Guides in writing variables:
	1. We can use 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
	2. Variable names should start with a lowercase character, use camelCase for multiple words

*/
// Best practices in nmaing variables:
// 1. When naming variables, it is important to create variables that are descriptive and indicative of the data it contains.

let firstName = "Jane" // good variable name
let lastName = "Doe" // good variable name
let pokemon = 25000 // bad variable name

// 2. It is better to start with a lower case letter. Because there are several keywords in JS that start in capital letter.

// 3. Do not add spaces to your variable names. Use camelCase for multiple words, or underscore.
	// let first name = "Mike"; <-- will cause error


// Declaring and initializing variables
// Initializing variables - the instance when a variable is given it's initial/starting value

let productName = "desktop computer";
console.log(productName);

let productPrice = 18999
console.log(productPrice);

const	interest =3.539;

// Reassigning variable values
productName ='laptop';
console.log(productName);

// let varaible can be reassigned with another value
// let variable cannot be re-declared within its scope

let friend = "Kate";
let anotherFriend = "John's laptop";
console.log(friend);

// const cannot be updated or re-declared
// value of constants cannot be changed and will simply return an error.
const pi = 3.14;
console.log(pi);

const anotherVariable = 1;


// Reassigning variables vs initializing variables
// let - we can declare a variable first

let supplier;
// initialization is done after the variable has been declared

supplier = "John Smith Tradings";
console.log(supplier);


// Reassign it's initial value
supplier = "Zuitt Store"; 
console.log(supplier);

// const variables are variables with constant data. Therefore we should not re-declare, re-assign or even declare a const variable without initialization.

// var vs. let/const

// var : Is also used in declaring a variable. var is an ECMA script (ES1) feature ES1 (JS 1997)
// let/const was introduced as a new feature in ES6 (2015)

// what makes let/const different from var?
	// Ther are issues when we used var in declaring variables, one of these is hoisting.
		// Hoising is a JS defualt behavior of moving declarations to the top.

a = 5;
console.log(a);
var a;

// Scope essentially means where these variables are available to use.
	// let/const can be used as a local/global scope
	// a block is chunk of code bounded by {}. A block lives in curly braces. Anything within curly braces is block.
	// So a variable declared in a blot with let/const is only available for use within that block.

// global scope
var outerVariable = "Hola";

{
// block/local scope = {}
	var innerVariable = "hello again";
	console.log(outerVariable);
}

// console.log(innerVariable); // is not defined



// Mutliple Variable Declarations
let productCode = "DC017";
const	productBrand ="Dell";
console.log(productCode, productBrand);


// DATA TYPES:
	// string, object, number, null, undefined, boolean
	// String : are a series characters that creates a word, phrase, sentenct or anything related to creating text. Strings in javascript can be written using either a single (') or double (") quotation.

let country = 'Philippines';
let province = "Metro Manila";
console.log(typeof country);
console.log(typeof province);

// Concatenating Strings
	// Multiple string values can be combined to create a single string using the "+" symbol.

let fullAddress = province + ', ' + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);


// Template literals (ES6) is the updated version of concatination
	// backticks `dvdv`
	// expression ${}

console.log(`I live in the ${province}, ${country}`)

// Escape character (\) in strings in combination with other characters can produce different effects
	// "\n" refers to creating a new line in between text

let mailAddress = "Metron Manila\n\nPhilippines";
console.log(mailAddress);

//  \'

let message = 'John\'s employees went home early';
console.log(message);

message = "John's employees went home early";
console.log(message);

console.log(

	`Metro Manila

	"Hello"
	'Helo'

Philippines ${province}`
	)


//NUMBERS
	// Integers or Whole numbers

let headCount =23;
console.log(typeof headCount);

	// Decimal numbers or Fractions

let grade = 98.7;
console.log(typeof grade);

	// Exponential notation

let planetDistance = 2e10;
console.log(typeof planetDistance);


// Combining variable with number data type and string
console.log("John's grade last quarty is " + grade);
console.log(`John's grade last quarter is ${grade}`);


// BOOLEAN
	// Boolean values are normall used to store values relating to the state of certain things.
	// This will be useful in further dicussions about creating logic to make our application respond to certain scenarios.


let isMarried = false;
let inGoodConduct = true;
console.log(typeof isMarried);
console.log(typeof inGoodConduct);


// OBJECT
	// Arrays are a special kind of data that is used to store multiple values It can store different different data types but is normally used to store simlar data types.

	// Similar data types
		// Syntax: let/const arrayName = [elementA, elementB, etc]

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);
console.log(typeof grades);

	// Different data types
		// Storing diff data types inside an array is not recommend because it will not make sense to join the context of programming.

let details = ["John", "Smith", 32, true];
console.log(details);

//indexing
console.log(grades[1], grades[0]);

// Reassigning array elements
	// let anime =['one piece','one punch man', 'attach on titan'];
	// console.log(anime);
	// anime[0] = 'kimetsu no yaiba';
	// console.log(anime);

const anime =['one piece','one punch man', 'attach on titan'];
console.log(anime);

anime[0] = 'kimetsu no yaiba';
console.log(anime);

// The keyword const is a little misleading. It does not define a constant value. It defines a constant reference to a value.

/*
Because of this you can not:
	Reassign a constant value, constant array, constant object

But you can:
	Change the element of a constant array
	Change the properties of constant object
*/


// OBJECTS
	// Almost the same with array
	// Syntax
	/*
		let/constant objectName = {propertyA: value, propoertyB: value}

		gives an individual peice of information and it is called a property of the object.

		They're used to create complex data that contains pieces of information that are relevant to each other.
	*/

let objectGrades = {
	firstQuarter: 98.7,
	secondQuarter: 92.1, 
	thirdQuarter: 90.2,
	fourthQuarter: 94.6
};
console.log(objectGrades);

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact:['09171234444','8123 4567'],
	address: {
		houseNumber:'345',
		city:'Manila'
	}
}
console.log(person);


//MINI ACTIVITY

	// Activity 1
let firstName_ma;
let lastName_ma;

firstName_ma = "Joshua";
lastName_ma = "Gayanelo";
console.log(`${firstName_ma} ${lastName_ma}`);

	// Activity 2
let sentence;

sentence = "I am a student of Zuitt student.";
console.log(`My name is, ${firstName_ma} ${lastName_ma} ${sentence}`);

	// Activity 3

let objectFavFod = ['Sinigang','Kare-kare','Steak','Sizzing Tofu'];
console.log(objectFavFod);

	// Activity 4

let person_ma = {
	firstName1: 'Joshua',
	lastName1: 'Gayanelo',
	isDeveloper: true,
	hasPortfolio: true,
	age: 30,
}
console.log(person_ma);


// NUMBER
let numb1 = 5;
let numb2 = 6;
let numb3 = 5.5;
let numb4 = .5;

let numString1 = "5";
let numString2 = "6";

console.log(numString1 + numString2); // "56" strings were concatinated
console.log(numb1 + numb2); // 11 

// TYPE COERCION/FORCED COERCION
	// is the automatic or implicit conversion of values from one data type to another
console.log(numString1 + numb1); // "55" resulted in concatenation
	// adding/concatinating a sting and a number will result into a string

// MATHEMATICAL OPERATIONS (+, -, *, /, %(modulo-remainder))
	
	// Module

console.log(numb1 % numb2); // (5/6) = remainder 5
console.log(numb2 % numb1); // (6/5) = remainder 1

console.log(numb2 - numb1)

let product = numb1 * numb2; 
console.log(product)

console.log(7%10);

// NULL
	// It is used to intentionally express the absence of a value in a variable declaration/initialization

let girlfriend = null;
console.log(girlfriend);

let myNumber = 0;
let myString = '';
	// Using null comparete a 0 value and an empty string is much better for readability purposes.


// UNDEFINED
	// Represents the state of a variable that has been declared but without an assign value.

let sampleVariable;
console.log(sampleVariable); // Undefined
	// The one clear differenc between undefined and null is that for Undefined, a variable was created but was not provided a value.
	// Null means that a variable was created and was assigned a value that does not hold any value or amount.



// FUNCTION
	// In JS, it is a line or lines or block of codes that tell our application to perform a certain task when called/invoked (mostly created to create complicated task).


// FUNCTION DECLARATION
	// Defines a function with the function and specified paramters.

/*

	Syntax:
		fyunction functionName() {
			code block/statement. -- the block of code will be executed once the function has been run/called/invoked.
		}
		
		function keyword => defines a JS function
		function name => name of the function - camelCase to name our function
		function block/statement => The statements which comprise the body of the function. This is where the code to be executed. 
*/

// function printName() {
// 	console.log("My name is John");
// };


// FUNCTION EXPRESSION
	// A function expression can be stored in a variable.

	// Anonymous function - a function without a name.
	let variableFunction = function() {
		console.log('Hello again serrr AHAHA');
	};

	let funcExpression = function func_name() {
		console.log("Hello!");
	};


// FUNCTION INVOCATION
	// The code inside a function is executed when the function is called/invoked.

// Let's call/invoke the function that we declared.

// printName();

function declaredFunction () {
	console.log("Hello World");
};

declaredFunction();

// How about the function expressions?
	// They are always invoked/call using the variable name.

variableFunction();

// FUNCTION SCOPING
	// JS Scope
		// Globack and local scope
	

//let faveCharacter = "Nezuko-chan"; // globeal scope

function myFunction() {
	let nickName = "Jane"; // function scope
	console.log(nickName);
	//console.log(faveCharacter);

};

myFunction();
// console.log(nickName); ----> not defined, out of scope

//console.log(faveCharacter);
//console.log(nickName);

// Variables defined inside a function are not accessbible (visible) from outside the function.
	// var/let/const are quite similar when declared inside a function.
// Variables declared globally (outside of the function) have a global scope or it can access anywhere.

function showSum(){
	console.log(6+6);
};

showSum();


// PARAMETERS AND ARGUMENTS
	// "name" is called a parameter
	// parameter => acts as a named variable/container that exists ONLY insded of the function. Usually paramters is the placeholder of an actual value.



function printName(name) {
	console.log(`My name is ${name}`);
};

	// Argument is the actual value/data that we passed to our function.

printName("Jane");



// MULTIPLE PARAMETERS AND ARGUMENTS

function displayFullName(fName, lName, age) {
	console.log(`${fName} ${lName} is ${age}`);
};

displayFullName("Judy", "Medalla", "jane", 25  );
	// Providing less arguments that the expected parameters will automatically assign an undefined value to the parameter. 
	// Providing more arugments will not affect our function.
	// We can also use a variable as an argument.


// Return Keyword
	// The "return" statement allows the output of a function to be passed to the block of code that invoked the function.
	// Any line or block of code that comes after ther return statement is ignored because it ends the function execution.
	// Return keyword is used so that a function may return a value.

	function createFullName(firstName, middleName, lastName) {
		return `${firstName} ${middleName} ${lastName}`
		console.log("I will no longer run because the function's value/result has been returned.")
	};

	// result of this function(return) can be saved into a variable
		let fullName2 = createFullName("Tom", "Cruise", "Mapother");
		console.log(fullName2);

	// The result of a function without a return keyword wil not save the result in a variable.

		let fullname3 = displayFullName("William", "Bradley", "Pitt");
		//console.log(fullName3);


		// let fullName4 = createFullName("Jeffrey," "John", "Smith");
		// console.log(fullName4);


/* MINI ACTIVITY
	Create a function that can receive two numbers as arguments.
		- display the quotient of the two numbers in the console.
		- return the value of the division

	Create a variable valled quotient.
		- this variable should be able to receive the result of division function

	Log the quotient variable's value in the console with the following message:
	"The result of the division is: <value of Qoutient>"
		- use concatenation/template literals

	Take a screenshot of your console and send it in the hangouts.
*/

		// Stretch goals 
		//prompt("what is your name");

function miniActivity(number1, number2){
	// return `${number1}` / `${number2}`;
	return(number1/number2);
};

let number1_input = prompt("Please enter your first number:", "");
let number2_input = prompt("Please enter your second number:", "");

let quotient = miniActivity(number1_input, number2_input);
console.log("The result of the division is " + quotient);

// function calcA(value1, value2, value3) {
// 	return (value1+value2+value3);
// };

// let result = calcA(4,123,3);
// console.log(result);